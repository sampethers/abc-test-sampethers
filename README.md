# Sam Pethers - ABC front-end coding test

## Technologies Used

- React
- Custom Bootstrap 4 install using only the grid, padding and margin utilities
- SCSS
- Javascript

## Running the project

```
1. npm install
2. npm start
3. project should run on localhost:3000
```

## Notes

### SCSS Structure

For the SCSS I decided to use Bootstrap 4 as it is well known by many developers and has now adopted the use of Flexbox for the grid system. I try to use any available Bootstrap utilities as much as possible to create standardised padding/margins for consistancy. 

For this task I created a custom Bootstrap build and commented out all un-nessessary files so that they can be added in future if needed.

All common/grid scss files can be found under the folder assets/scss. These files are required throughout the site and can be re-used anywhere. This is to try to avoid code repeating wherever possible.

The only file which is solely for the 'Card' component is under components/Card/index.scss. This file uses BEM naming specific for the 'Card' component.

### Card Component

#### JSON File

- The json file holding the events data can be found under data/events.json

#### Javascript functions

- I have created a helpers.js file under assets/scripts/helpers.js. This is used to hold re-usable javascript functions. For this task it has three functions.
```
1. Convert Date function: Converts the date posted from a timeStamp to seconds/minutes/hours ago.
2. Randomize number function: This is to choose an event at random from the json file.
3. Convert Time function: Convert the time length of the video from seconds to minutes/seconds.
```

#### Post Request

- For the post request I chose to send the data from the event button to a sample server I found online at https://requestb.in/10winrc1. This will have expired by the time you check the test.

### Sketch

My trial version of Sketch has expired so I was unable to open the sketch file. I did my best to
estimate font sizes, weights etc.

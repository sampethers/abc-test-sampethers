import React from 'react';
import { randomNumber } from '../../assets/scripts/helpers.js';
import { convertTime } from '../../assets/scripts/helpers.js';
import { timeSince } from '../../assets/scripts/helpers.js';
import eventsData from '../../data/events.json';
import drumIcon from '../../assets/img/drum.png';
import saveIcon from '../../assets/img/save.svg';
import commentIcon from '../../assets/img/comments.svg';
import shareIcon from '../../assets/img/share.svg';
import playIcon from '../../assets/img/play.svg';
import plusIcon from '../../assets/img/add.svg';

import './index.scss'

class Card extends React.Component {
    constructor(){
        super();
        this.state = {}
    }

    handleSubmit(e) {
        e.preventDefault();
        // Add form action into addEvent variable
        const addEvent = {
            event: this.eventForm.action
        };

        // Post to sample server
        fetch('https://requestb.in/10winrc1', {
            method: 'POST',
            mode: 'no-cors',
            headers: {'Accept': 'application/json'},
            body: JSON.stringify(addEvent)
        }).then((data) => {
            console.log('Request Success', data);
        }).catch((error) => {
            console.log('Request failure:', error); 
        });

    }

    displayEvents() {

        // Create empty eventsList array to store events
        const eventsList = [];

        // Push events into empty EventsList array
        eventsList.push(...eventsData.events);

        // Get random object from eventsList array using helper randomize function        
        const randomEvent = randomNumber(eventsList);

        // Convert duration to minutes using helper convertTime function
        const durationToMinutes = convertTime(randomEvent.duration)

        // Convert time posted timestamp to time ago using helper timeSince function
        const timeSincePost = timeSince(randomEvent.timePosted);
        
        // Create object of randomly selected event
        const event = {
            image: randomEvent.image,
            imagex2: randomEvent.imagex2,
            duration: durationToMinutes,
            title: randomEvent.title,
            content: randomEvent.content,
            eventLink: randomEvent.eventLink,
            presenterName: randomEvent.presenterName,
            timePosted: timeSincePost,
            tag: randomEvent.tag
        }
        
        // Update the state of each variable
        this.setState({
            image: event.image,
            imagex2: event.imagex2,
            duration: event.duration,
            title: event.title,
            content: event.content,
            eventLink: event.eventLink,
            presenterName: event.presenterName,
            timePosted: event.timePosted,
            tag: event.tag
        });
    }
    componentDidMount() {
        // Initialise displayEvents function to get random event from json file.
        this.displayEvents();
        // Run displayEvents function every 10 seconds
        setInterval(() => {
            this.displayEvents();
        }, 10000);
    }
    render(){
        return (
            <div className="card u-force-center mt-3">
                <div className="p-2 p-sm-3">
                    <div className="card__image u-pos-rel">
                        <div className="card__live-tag">
                            <div className="px-3 py-1">
                                <strong className="font--small font--bold font--uppercase">
                                    Live
                                </strong>
                            </div>
                        </div>
                        <img 
                            src={this.state.image} 
                            srcSet={this.state.image + ' 1.5x,' + this.state.imagex2 + ' 2x'}
                            alt={this.state.title}
                            title={this.state.title}
                            className="img-fluid"
                        />
                        <div className="card__play-btn-wrapper d-flex">
                            <div className="card__drum-icon align-items-center d-flex py-2 py-md-3">
                                <img 
                                    src={drumIcon} 
                                    className="ml-3 mr-4" 
                                    alt="Triple J Drum"
                                    title="Triple J Drum"
                                />
                            </div>
                            <div className="card__play-btn px-2 d-flex">
                                <div className="d-flex align-items-center pr-2 pr-sm-4">
                                    <button className="btn btn--normalize">
                                        <img 
                                            src={playIcon} 
                                            alt="Play Button"
                                            title="Play Button"
                                        />
                                    </button>
                                </div>
                                <div className="d-flex align-items-center pr-3">
                                    <span className="fc--white font--small">
                                        <strong>{this.state.duration}m</strong>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="card__content pt-3">
                        <h2 className="font--title-1 font--bold">{this.state.title}</h2>
                        <div className="pt-1">
                            <p>{this.state.content}</p>
                        </div>
                        <form 
                            ref={(input) => this.eventForm = input} 
                            method="post" 
                            onSubmit={(e) => this.handleSubmit(e)}
                            action={this.state.eventLink}
                        >
                            <button action={this.state.eventLink} 
                                className="btn btn--icon d-flex my-4 py-3 px-5 u-force-center" 
                                type="submit"
                            >
                                <img 
                                    src={plusIcon} 
                                    className="mr-3" 
                                    alt="Plus Icon"
                                    title="Plus Icon"
                                />
                                <span className="font--uppercase">Follow this event</span>
                            </button>
                        </form>
                        <div className="card__details">
                            <p className="font--small">Presented by <strong>{this.state.presenterName}</strong> / {this.state.timePosted} ago / <strong>{this.state.tag}</strong></p>
                        </div>
                    </div>
                </div>
                <div className="card__share d-flex py-2 py-sm-3 px-2 px-sm-4">
                    <div className="pr-3 pr-sm-5">
                        <button className="btn btn--normalize font--small">
                            <img 
                                src={saveIcon} 
                                className="card__share-icon pr-1 pr-sm-2" 
                                alt="Save Icon"
                                title="Save Icon"
                            /> 
                            <span>Save</span>                                        
                        </button>
                    </div>
                    <div className="pr-3 pr-sm-5">
                        <button className="btn btn--normalize font--small">
                            <img 
                                src={commentIcon} 
                                className="card__share-icon pr-1 pr-sm-2" 
                                alt="Comments Icon" 
                                title="Comments Icon"
                            />
                            <span>27</span>
                        </button>
                    </div>
                    <div className="pr-3 pr-sm-5">
                        <button className="btn btn--normalize font--small">
                            <img 
                                src={shareIcon} 
                                className="card__share-icon pr-1 pr-sm-2" 
                                alt="Share Icon"
                                title="Share Icon"
                            />
                            <span>Share</span>
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}

export default Card;
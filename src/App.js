import React, { Component } from 'react';
import Card from './components/Card';
import './assets/scss/main.scss';

class App extends Component {
  render() {
    return (
      <main>
          <div className="container">
              <div className="row">
                  <div className="col-sm-12">
                      <Card/>
                  </div>
              </div>
          </div>
      </main>
    );
  }
}

export default App;

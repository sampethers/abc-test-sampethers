// Convert time posted timestamp to time ago
export function timeSince(date) {
    var seconds = Math.floor((new Date() - date) / 1000);

    var interval = Math.floor(seconds / 31536000);

    if (interval > 1) {
        return interval + "y";
    }
    interval = Math.floor(seconds / 2592000);
    if (interval > 1) {
        return interval + "months";
    }
    interval = Math.floor(seconds / 86400);
    if (interval > 1) {
        return interval + "d";
    }
    interval = Math.floor(seconds / 3600);
    if (interval > 1) {
        return interval + "h";
    }
    interval = Math.floor(seconds / 60);
    if (interval > 1) {
        return interval + "m";
    }
    return Math.floor(seconds) + "s";
}

// Randomize number
export function randomNumber(randomize){
    return randomize[Math.floor(Math.random() * randomize.length)];
}

// Convert time in seconds to minutes/seconds

export function convertTime(timeInSeconds){
    const durationMinutes = Math.floor(timeInSeconds / 60);
    const durationSeconds = timeInSeconds - durationMinutes * 60
    const durationToMinutes = durationMinutes + ':' + durationSeconds;
    return durationToMinutes;
}
